import plugin from '../../lib/plugins/plugin.js';  
import fetch from 'node-fetch';  
  
export class Constellation extends plugin {  
  constructor() {  
    super({  
      name: '星座运势查询',  
      dsc: '提供每日星座运势查询服务',  
      event: 'message',  
      priority: 5000,  
      rule: [  
        {  
          reg: `^#(.*)(星座运势查询)(.*)`, // 假设用户发送的消息格式为 '#星座运势查询 [星座] [时间类型]'  
          fnc: 'getConstellation'  // 处理函数名  
        },  
      ]  
    });  
  }  
  
  async getConstellation(e) {  
    let msg = e.msg;  
    // 解析用户输入的星座和时间类型  
    let [sign, timeType] = msg.replace(/#|星座运势查询/g, "").trim().split(" ");  
    
    // 将中文星座名称转换为英文小写  
    let englishSign = this.convertChineseSignToEnglish(sign);  
    if (!englishSign) {  
      // 如果转换失败，向用户发送错误消息并返回  
      await this.reply('无效的星座名称，请输入正确的星座名称。');  
      return;  
    }  
    
    // 构造API请求URL  
    let url = `http://api.suxun.site/api/constellation?type=${englishSign}&time=${timeType.toLowerCase()}`;  
    console.log('API请求URL:', url); // 打印API请求URL  
    
    try {  
      let response = await fetch(url);  
      console.log('API响应状态码:', response.status); // 打印API响应状态码  
      let data = await response.json(); // 将响应解析为JSON对象  
    
      if (response.ok) { // 检查响应是否成功（状态码200-299）  
        if (data.code === '200') {  
          // 成功获取数据，开始解析和显示  
          let constellationData = data.data;  
        await this.reply(`您的${sign}星座${timeType}运势如下：`);  
        await this.reply(`整体运势: ${constellationData.all}%`);  
        await this.reply(`综合运势文本: ${constellationData.all_text}`);  
    
        // 显示爱情运势  
        await this.reply(`爱情运势: ${constellationData.love}%`);  
        await this.reply(`爱情运势文本: ${constellationData.love_text}`);  
    
        // 显示工作运势  
        await this.reply(`工作运势: ${constellationData.work}%`);  
        await this.reply(`工作运势文本: ${constellationData.work_text}`);  
    
        // 显示财运  
        await this.reply(`财运: ${constellationData.money}%`);  
        await this.reply(`财运文本: ${constellationData.money_text}`);  
    
        // 显示健康运势  
        await this.reply(`健康运势: ${constellationData.health}%`);  
        await this.reply(`健康运势文本: ${constellationData.health_text}`);  
    
        // 显示其他信息  
        await this.reply(`今日宜: ${constellationData.yi}`);  
        await this.reply(`今日忌: ${constellationData.ji}`);  
        await this.reply(`幸运星座: ${constellationData.lucky_star}`);  
        await this.reply(`幸运颜色: ${constellationData.lucky_color}`);  
        await this.reply(`幸运数字: ${constellationData.lucky_number}`);  
        await this.reply(`注意事项: ${constellationData.notice}`);  
        await this.reply(`讨论运势: ${constellationData.discuss}%`);  
    // 显示整体运势等（这里省略了具体的显示逻辑，因为与原始代码相同）  
        } else {  
          // API返回了错误代码，处理错误  
          console.error('API返回错误代码:', data.code, '错误信息:', data.message);  
          await this.reply('星座运势查询失败，请稍后再试或联系支持');  
        }  
      } else {  
        // 请求失败，可能是网络问题或API返回了错误状态码  
        console.error('API请求失败，状态码:', response.status);  
        let errorText = '星座运势查询接口请求失败, 请稍后再试或联系支持';  
        if (response.status === 404) {  
          errorText = 'API接口未找到，请检查API地址是否正确';  
        }  
        await this.reply(errorText);  
      }  
    } catch (error) {  
      // 如果发生错误，记录并回复用户  
      console.error('捕获到错误:', error); // 打印错误信息和堆栈跟踪  
      logger.error('[星座运势查询] 发生错误:', error);  
      await this.reply('星座运势查询接口请求失败, 请稍后再试或联系支持');  
    }  
  }
    
  // 将中文星座名称转换为英文小写  
  convertChineseSignToEnglish(chineseSign) {  
    const chineseToEnglish = {  
      '白羊座': 'aries',  
      '金牛座': 'taurus',  
      '双子座': 'gemini',  
      '巨蟹座': 'cancer',  
      '狮子座': 'leo',  
      '处女座': 'virgo',  
      '天秤座': 'libra',  
      '天蝎座': 'scorpio',  
      '射手座': 'sagittarius',  
      '摩羯座': 'capricorn',  
      '水瓶座': 'aquarius',  
      '双鱼座': 'pisces' // 注意：你提供的列表中没有'pisces'，但我添加了它因为它是十二星座的一部分  
    };  
    return chineseToEnglish[chineseSign];  
  }
}